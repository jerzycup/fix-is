# frozen_string_literal: true

# Student
class Student
  def initialize(name)
    @first_name, @last_name = name.split(' ')
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def name=(name)
    @first_name, @last_name = name.split(' ')
  end

  def to_s
    "#{@first_name} #{@last_name}"
  end
end
