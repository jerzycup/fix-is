# frozen_string_literal: true

# Student grade
class Grade
  attr_reader :number, :date
  def initialize(number, date)
    @date = date
    @number = number
  end

  def to_s
    "#{@number} on #{@date}"
  end
end
